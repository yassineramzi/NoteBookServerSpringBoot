package com.notebook.endpoints.exceptionsHandlers;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.notebook.core.exceptions.ApiException;
import com.notebook.core.exceptions.CompilationError;
import com.notebook.core.exceptions.TimeOutException;
import com.notebook.core.exceptions.UnknownInterpreter;

@ControllerAdvice
public class ApiErrorsHandler extends ResponseEntityExceptionHandler
{
	@ExceptionHandler({ CompilationError.class })
	public ResponseEntity<Object> handleCompilatioError(final CompilationError compilationError)
	{
		ApiError apiError = getErrorApi(compilationError);
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}
	
	@ExceptionHandler({ UnknownInterpreter.class })
	public ResponseEntity<Object> handleUnknownInterpreter(final UnknownInterpreter unknownInterpreter)
	{
		ApiError apiError = getErrorApi(unknownInterpreter);
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}
	
	@ExceptionHandler({ TimeOutException.class })
	public ResponseEntity<Object> handleTimeOutException(final TimeOutException timeOutException)
	{
		ApiError apiError = getErrorApi(timeOutException);
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}
	
	public ApiError getErrorApi(final ApiException apiException)
	{
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, apiException.getErrorName(), apiException.getMessage());
		return apiError;
	}
}

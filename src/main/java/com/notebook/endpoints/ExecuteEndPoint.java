package com.notebook.endpoints;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.notebook.core.exceptions.CompilationError;
import com.notebook.core.exceptions.TimeOutException;
import com.notebook.core.exceptions.UnknownInterpreter;
import com.notebook.models.CodeExpression;
import com.notebook.models.Result;
import com.notebook.service.CodeExecuteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
@Api(value="Note book server")
public class ExecuteEndPoint {


	@Autowired
	private CodeExecuteService codeExecuteService;
	
	
	@ApiOperation(value = "Execute code", response = Iterable.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Code successfully executed"),
	        @ApiResponse(code = 401, message = "Compilation error")
	}
	)
    @RequestMapping(value="/execute", method = RequestMethod.POST)
    public Result execute(HttpServletRequest request,@RequestBody CodeExpression code) throws CompilationError, UnknownInterpreter, TimeOutException
    {	
    	return codeExecuteService.executeCode(request.getHeader("sessionId"),code);
    }
    
}

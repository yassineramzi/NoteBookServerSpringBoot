package com.notebook.core.writer.impl;

import java.io.StringWriter;

import org.springframework.stereotype.Component;

import com.notebook.core.writer.NoteBookStreamWriter;

@Component
public class NoteBookStreamWriterImpl implements NoteBookStreamWriter
{
	private StringWriter writer;
	
	public NoteBookStreamWriterImpl() 
	{
		this.writer = new StringWriter();
	}

	@Override
	public String getStreamWrited() 
	{
		String result = writer.toString();
		writer.getBuffer().setLength(0);
		return result.replace("\n", "");
	}

	@Override
	public StringWriter getWriter() {
		return writer;
	}

	
}

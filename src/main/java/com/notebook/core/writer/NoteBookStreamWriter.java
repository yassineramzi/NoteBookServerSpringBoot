package com.notebook.core.writer;

import java.io.StringWriter;

public interface NoteBookStreamWriter
{
	String getStreamWrited();
	StringWriter getWriter();
}

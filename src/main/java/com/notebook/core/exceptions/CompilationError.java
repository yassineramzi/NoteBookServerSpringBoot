package com.notebook.core.exceptions;

public class CompilationError extends ApiException 
{

	private static final long serialVersionUID = 8828964100571163634L;

	public CompilationError()
	{
		super("COMPILATION_ERROR");
		
	}
	
	public CompilationError(final String message)
	{
		super("COMPILATION_ERROR",message);
	}

}

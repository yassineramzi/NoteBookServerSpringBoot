package com.notebook.core.exceptions;

public class UnknownInterpreter extends ApiException
{

	private static final long serialVersionUID = -3287797927891727028L;

	public UnknownInterpreter() 
	{
		super("UNKNOWN_INTEPRETER");
	}

	public UnknownInterpreter(final String message)
	{
		super("UNKNOWN_INTEPRETER", message);
	}
}

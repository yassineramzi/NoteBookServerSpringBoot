package com.notebook.core.exceptions;

public class TimeOutException extends ApiException
{
	private static final long serialVersionUID = 4392234004026119140L;
	
	public TimeOutException()
	{
		super("EXECUTION_TIME_OUT");
	}
	
	public TimeOutException(final String message)
	{
		super("EXECUTION_TIME_OUT",message);
	}
}

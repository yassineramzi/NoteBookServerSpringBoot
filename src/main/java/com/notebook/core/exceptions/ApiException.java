package com.notebook.core.exceptions;

public class ApiException extends Exception{

	private static final long serialVersionUID = -6034935007174492808L;
	
	private String errorName;
	
	private String message;
	
	public ApiException(String errorName)
	{
		this.errorName = errorName;
		this.setMessage(null);
	}

	public ApiException(final String errorName, final String message) 
	{
		this.errorName = errorName;
		this.message = message;
	}

	public String getErrorName() {
		return errorName;
	}

	public void setErrorName(String errorName) {
		this.errorName = errorName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	
	
}

package com.notebook.core.executor.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.notebook.core.exceptions.UnknownInterpreter;
import com.notebook.core.executor.Executor;


@Component("python")
@Scope("prototype")
public class PythonExecutor extends Executor
{
	
	public PythonExecutor() throws UnknownInterpreter
	{
		super("jython");
	}
	
}

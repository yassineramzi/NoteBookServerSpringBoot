package com.notebook.core.executor;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.notebook.core.exceptions.CompilationError;
import com.notebook.core.exceptions.TimeOutException;
import com.notebook.core.exceptions.UnknownInterpreter;
import com.notebook.core.writer.NoteBookStreamWriter;
import com.notebook.models.Program;
import com.notebook.models.Result;

public abstract class Executor
{

	protected ScriptEngine scriptEngine;
    
    protected String interpreterName;
    
    protected NoteBookStreamWriter noteBookStreamWriter;
    
    protected long timeOut;
	
	public Executor(final String name) throws UnknownInterpreter
	{
		if(name == null)
		{
			throw new UnknownInterpreter();
		}
		else
		{
			this.interpreterName = name;
		}
	}
	
	//execute inside thread start the streaming when with thread and close it with it + add some timeout to execute the script inside the thread
	public Result eval(final Program program) throws CompilationError, UnknownInterpreter, TimeOutException
	{
		Result result = new Result();
		CompilationError compilationError = new CompilationError();
		// run the code inside thread with a timeout
		TimeOutController.execute(new Runnable() {

			@Override
			public void run() {
				try
				{
					scriptEngine.eval(program.getCode());
					result.setResult(noteBookStreamWriter.getStreamWrited());
				}
				catch(ScriptException e)
				{
					compilationError.setMessage(e.getMessage());
				}
			}
			
		}, timeOut);
		// throw CompilationError if an exception was returned
		if(compilationError.getMessage() != null)
		{
			throw compilationError;
		}
		return result;
	}

	public String getInterpreterName()
	{
		return interpreterName;
	}

	public void setInterpreterName(String interpreterName) 
	{
		this.interpreterName = interpreterName;
	}

	public ScriptEngine getScriptEngine()
	{
		return scriptEngine;
	}

	public void setScriptEngine(ScriptEngine scriptEngine)
	{
		this.scriptEngine = scriptEngine;
		ScriptContext context = this.scriptEngine.getContext();
		context.setWriter(noteBookStreamWriter.getWriter());
	}
	
	public NoteBookStreamWriter getNoteBookStreamWriter()
	{
		return noteBookStreamWriter;
	}
	
	@Autowired
	public void setNoteBookStreamWriter(
			final NoteBookStreamWriter noteBookStreamWriter) 
	{
		this.noteBookStreamWriter = noteBookStreamWriter;
	}

	public long getTimeOut()
	{
		return timeOut;
	}
	
    @Value("${execute.timeout}")
	public void setTimeOut(long timeOut) 
    {
		this.timeOut = timeOut;
	}
	
	
}

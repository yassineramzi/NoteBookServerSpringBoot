package com.notebook.core.executor;

import java.util.HashMap;
import java.util.Map;

import javax.script.ScriptEngineManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.notebook.core.exceptions.UnknownInterpreter;

@Component
public class ExecutorFactory
{
	@Autowired
	private ApplicationContext context;
	
    private ScriptEngineManager manager;
    
    private Map<String, Executor> executors;
    
    public ExecutorFactory()
    {
    	this.manager = new ScriptEngineManager();
    	this.executors = new HashMap<String, Executor>();
    }

	public Executor getScriptEngineBySessionId(final String name, final String sessionId ) throws UnknownInterpreter
	{
		String idExecutor = name+sessionId;
		Executor temp = null;
		if(sessionId != null)
		{
			temp = executors.get(idExecutor);
			if(temp == null)
			{
				temp =  getNewScriptEngine(name);
				executors.put(idExecutor, temp);
			}
		}
		else
		{
			temp = getNewScriptEngine(name);
		}
		return temp;
	}
	
	public Executor getNewScriptEngine(final String name) throws UnknownInterpreter
	{
		Executor executor = null;
		try
		{
			executor = (Executor) context.getBean(name, Executor.class);
			executor.setScriptEngine(this.manager.getEngineByName(executor.getInterpreterName()));
		}
		catch(Exception e)
		{
			throw new UnknownInterpreter(e.getMessage());
		}
		return executor;
	}
}

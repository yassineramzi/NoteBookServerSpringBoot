package com.notebook.core.parser;

import com.notebook.models.CodeExpression;
import com.notebook.models.Program;

public interface CodeParser 
{
	Program parseCode(final CodeExpression codeExpression);
	
	String getInterpreter(final String codeExpression);
	
	String getCode(final String codeExpression);
}

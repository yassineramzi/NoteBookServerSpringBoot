package com.notebook.core.parser.impl;

import org.springframework.stereotype.Component;

import com.notebook.core.parser.CodeParser;
import com.notebook.models.CodeExpression;
import com.notebook.models.Program;

@Component
public class CodeParserImpl implements CodeParser 
{
	
	@Override
	public Program parseCode(CodeExpression codeExpression) 
	{
		String code = codeExpression.getCode();
		Program program = new Program();
		program.setCode(this.getCode(code));
		program.setInterpreter(this.getInterpreter(code));
		return program;
	}
	
	@Override
	public String getInterpreter(final String code)
	{
		StringBuilder interpreter = new StringBuilder();
		if(code != null)
		{
			int endIndex = code.indexOf(" ");
			interpreter.append(code.subSequence(0, endIndex));
			interpreter.replace(0, 1, "");
		}
		return interpreter.toString();
	}
	
	@Override
	public String getCode(final String code) 
	{
		StringBuilder finalCode = new StringBuilder();
		if(code != null)
		{
			int beginIndex = code.indexOf(" ")+1;
			finalCode.append(code.substring(beginIndex));
		}
		return finalCode.toString();
	}

}

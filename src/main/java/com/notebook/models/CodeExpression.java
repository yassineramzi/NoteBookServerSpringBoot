package com.notebook.models;

import io.swagger.annotations.ApiModelProperty;

public class CodeExpression {
	
	@ApiModelProperty(notes = "The code to execute", required=true, example="%python print 1+1")
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
}

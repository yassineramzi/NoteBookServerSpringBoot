package com.notebook.models;

public class Program {
	
	private String interpreter;
	
	private String code;
	

	public String getCode() 
	{
		return code;
	}

	public void setCode(String code) 
	{
		this.code = code;
	}

	public String getInterpreter() 
	{
		return interpreter;
	}

	public void setInterpreter(String interpreter) 
	{
		this.interpreter = interpreter;
	}
	
	
}

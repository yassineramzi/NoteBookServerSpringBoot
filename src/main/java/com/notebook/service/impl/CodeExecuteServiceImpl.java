package com.notebook.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.notebook.core.exceptions.CompilationError;
import com.notebook.core.exceptions.TimeOutException;
import com.notebook.core.exceptions.UnknownInterpreter;
import com.notebook.core.executor.Executor;
import com.notebook.core.executor.ExecutorFactory;
import com.notebook.core.parser.CodeParser;
import com.notebook.models.CodeExpression;
import com.notebook.models.Program;
import com.notebook.models.Result;
import com.notebook.service.CodeExecuteService;


@Component
public class CodeExecuteServiceImpl implements CodeExecuteService 
{
	private Executor executor;

	@Autowired
	private CodeParser codeParser;
	
	@Autowired
	private ExecutorFactory executorFactory;
	
	@Override
	public Result executeCode(final String sessionId, final CodeExpression code) throws CompilationError, UnknownInterpreter, TimeOutException 
	{
		Program program = codeParser.parseCode(code);
    	// Create executor based on the user input
    	executor = executorFactory.getScriptEngineBySessionId(program.getInterpreter(),sessionId);
		return executor.eval(program);
	}

	public Executor getExecutor() 
	{
		return executor;
	}

	public void setExecutor(Executor executor) 
	{
		this.executor = executor;
	}
	
	
}

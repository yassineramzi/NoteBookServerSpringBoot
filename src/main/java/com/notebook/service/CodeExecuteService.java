package com.notebook.service;

import com.notebook.core.exceptions.CompilationError;
import com.notebook.core.exceptions.TimeOutException;
import com.notebook.core.exceptions.UnknownInterpreter;
import com.notebook.models.CodeExpression;
import com.notebook.models.Result;

public interface CodeExecuteService
{
	Result executeCode(final String sessionId, final CodeExpression code) throws CompilationError, UnknownInterpreter, TimeOutException;
}

package com.notebook.core.executor.impl;

import static org.junit.Assert.assertEquals;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import com.notebook.core.exceptions.CompilationError;
import com.notebook.core.exceptions.UnknownInterpreter;
import com.notebook.core.executor.Executor;
import com.notebook.core.executor.ExecutorFactory;
import com.notebook.core.writer.NoteBookStreamWriter;
import com.notebook.models.Program;
import com.notebook.models.Result;

@RunWith(SpringRunner.class)
public class PythonExecutorTest 
{
	@TestConfiguration
	static class PythonExecutorTestContextConfiguration 
	{
		@Bean
		public ExecutorFactory executorFactory() throws UnknownInterpreter
		{
			return new ExecutorFactory();
		}
		
		@Bean(name="python")
		public PythonExecutor executor() throws UnknownInterpreter
		{
			return new PythonExecutor();
		}
		
		@Bean
		public static PropertySourcesPlaceholderConfigurer properties() 
		{
		  final PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
		  pspc.setLocation(new ClassPathResource("application.properties"));
		  return pspc;
		}
	}
	
	@Autowired
	private ExecutorFactory executorFactory;
	
	@MockBean
	private NoteBookStreamWriter streamWriter;
	
	@MockBean
	private ScriptEngine scriptEngine;
	
	@MockBean
	private ScriptContext scriptContext;
	
	private Executor pythonExecutor;
	
	private String result;
	
	private Program program;
	
	private Result formattedResult;

	private void beforePythonExecutorTestSuccess() throws UnknownInterpreter, ScriptException
	{
		result = "2";
		
		formattedResult = new Result();
		formattedResult.setResult(result);
		
		program = new Program();
		program.setCode("print 1+1");
		
		Mockito.when(streamWriter.getStreamWrited()).thenReturn(result);
		Mockito.when(scriptEngine.eval(program.getCode())).thenReturn(null);
		Mockito.when(scriptEngine.getContext()).thenReturn(scriptContext);
		
		pythonExecutor = executorFactory.getNewScriptEngine("python");	
		pythonExecutor.setScriptEngine(scriptEngine);
	}
	
	@Test
	public void testEvalSuccess() throws Exception
	{
		/** Before */
		this.beforePythonExecutorTestSuccess();
		/** When */
		Result expectedResult = pythonExecutor.eval(program);
		/** Then */
		assertEquals(result,expectedResult.getResult());
	}
	
	private void beforePythonExecutorTestUnknownInterpreter() throws Exception
	{		
		Mockito.when(streamWriter.getStreamWrited()).thenReturn(result);
		
		pythonExecutor = executorFactory.getNewScriptEngine("python");
	}
	
	@Test (expected = UnknownInterpreter.class)
	public void testEvalUnknownInterpreter() throws Exception
	{
		/** Before */
		this.beforePythonExecutorTestUnknownInterpreter();
		/** When */
		pythonExecutor = executorFactory.getNewScriptEngine("unkonwn");	
	}
	
	private void beforePythonExecutorTestCompilationError() throws Exception
	{		
		result = "2";
		
		formattedResult = new Result();
		formattedResult.setResult(result);
		
		program = new Program();
		program.setCode("print 1+1");
		
		Mockito.when(streamWriter.getStreamWrited()).thenReturn(result);
		Mockito.when(scriptEngine.eval(program.getCode())).thenThrow(new ScriptException("test script exception"));
		Mockito.when(scriptEngine.getContext()).thenReturn(scriptContext);
		
		pythonExecutor = executorFactory.getNewScriptEngine("python");	
		pythonExecutor.setScriptEngine(scriptEngine);
	}
	
	@Test (expected = CompilationError.class)
	public void testEvalCompilationError() throws Exception
	{
		/** Before */
		this.beforePythonExecutorTestCompilationError();
		/** When */
		pythonExecutor.eval(program);	
	}
}

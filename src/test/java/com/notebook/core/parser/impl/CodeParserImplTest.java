package com.notebook.core.parser.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.notebook.core.parser.CodeParser;
import com.notebook.models.CodeExpression;
import com.notebook.models.Program;


@RunWith(SpringRunner.class)
public class CodeParserImplTest 
{
	@TestConfiguration
	static class CodeParserImplTestContextConfiguration 
	{
		@Bean
		public CodeParserImpl codeParser()
		{
			return new CodeParserImpl();
		}
	}
	
	@Autowired
	private CodeParser codeParser;
	
	private String codeExpression;
	
	private Program program;
	
	private CodeExpression expressionCode;
	
	@Before
	public void beforeTest()
	{
		/** Before */
		codeExpression = "%python print 1+1";
		
		program = new Program();
		program.setCode("print 1+1");
		program.setInterpreter("python");
		
		expressionCode = new CodeExpression();
		expressionCode.setCode(codeExpression);
	}
	
	@Test
	public void testParserGetIntepreter()
	{
		/** When */
		String interpreter = codeParser.getInterpreter(codeExpression);
		/** Then */
		assertEquals("python",interpreter);
	}
	
	@Test
	public void testParserGetCode()
	{
		/** When */
		String code = codeParser.getCode(codeExpression);
		/** Then */
		assertEquals("print 1+1",code);
	}
	
	@Test
	public void testParserGetProgram()
	{
		/** When */
		Program returnedProgram = codeParser.parseCode(expressionCode);
		/** Then */
		assertEquals(returnedProgram.getInterpreter(),program.getInterpreter());
		assertEquals(returnedProgram.getCode(),program.getCode());
	}
}

package com.notebook.core.writer.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.notebook.core.writer.NoteBookStreamWriter;

@RunWith(SpringRunner.class)
public class NoteBookStreamWriterImplTest 
{
	@TestConfiguration
	static class NoteBookStreamWriterImplTestContextConfiguration 
	{
		@Bean
		public NoteBookStreamWriter codeExecuteService()
		{
			return new NoteBookStreamWriterImpl();
		}
	}
	@Autowired
	private NoteBookStreamWriter noteBookStreamWriter;
	
	@Test
	public void testGetStreamWrited()
	{
		/** Before */
		noteBookStreamWriter.getWriter().write("2\n");
		/** When */
		String result = noteBookStreamWriter.getStreamWrited();
		/** Then */
		assertEquals("2",result);
	}
}

package com.notebook.endpoints;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.notebook.core.exceptions.CompilationError;
import com.notebook.core.exceptions.TimeOutException;
import com.notebook.core.exceptions.UnknownInterpreter;
import com.notebook.models.CodeExpression;
import com.notebook.models.Result;
import com.notebook.service.CodeExecuteService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ExecuteEndPoint.class, secure = false)
public class ExecuteEndPointTest 
{
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private CodeExecuteService codeExecuteService;
	
	private Result mockResult;
	
	private String jsonResult;
	
	private String jsonRequest;
	
	private void beforeExecuteServiceSuccess()
	{
		mockResult = new Result();
		mockResult.setResult("2");
		jsonResult = "{\"result\":\"2\"}";
		jsonRequest = "{\"code\":\"%python print 1+1\"}";
		try 
		{
			Mockito.when(
					codeExecuteService.executeCode(Mockito.anyString(),
							Mockito.any(CodeExpression.class))).thenReturn(mockResult);
		} 
		catch (CompilationError | UnknownInterpreter | TimeOutException e) 
		{

		}
	}
	
	@Test
	public void testExecuteServiceSuccess() throws Exception
	{
		/** Before */
		this.beforeExecuteServiceSuccess();
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/execute")
				.accept(MediaType.APPLICATION_JSON)
				.content(jsonRequest)
				.contentType(MediaType.APPLICATION_JSON);
		/** When */
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		/** Then */
		JSONAssert.assertEquals(jsonResult, result.getResponse().getContentAsString(),false);
	}
	
	private void beforeExecuteServiceUnknownInterpreter()
	{
		UnknownInterpreter exception = new UnknownInterpreter();
		exception.setMessage("No bean named 'pythoa' available");
		jsonResult = "{" + 
				"\"status\":\"BAD_REQUEST\"," + 
				"\"message\":\"UNKNOWN_INTEPRETER\"," + 
				"\"error\":\"No bean named 'pythoa' available\"" + 
				"}";
		jsonRequest = "{\"code\":\"%pythoa print a+1\"}";
		try 
		{
			Mockito.when(
					codeExecuteService.executeCode(Mockito.anyString(),
							Mockito.any(CodeExpression.class))).thenThrow(exception);
		} 
		catch (CompilationError | UnknownInterpreter | TimeOutException e) 
		{

		}
	}
	
	@Test
	public void testExecuteServiceUnknownInterpreter() throws Exception
	{
		/** Before */
		this.beforeExecuteServiceUnknownInterpreter();
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/execute")
				.accept(MediaType.APPLICATION_JSON)
				.content(jsonRequest)
				.contentType(MediaType.APPLICATION_JSON);
		/** When */
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		/** Then */
		JSONAssert.assertEquals(jsonResult, result.getResponse().getContentAsString(),false);
	}
	
	private void beforeExecuteServiceCompilationError()
	{
		CompilationError exception = new CompilationError();
		exception.setMessage("NameError: name 'a' is not defined in <script> at line number 1");
		jsonResult = "{" + 
				"\"status\":\"BAD_REQUEST\"," + 
				"\"message\":\"COMPILATION_ERROR\"," + 
				"\"error\":\"NameError: name 'a' is not defined in <script> at line number 1\"" + 
				"}";
		jsonRequest = "{\"code\":\"%python print a+1\"}";
		try 
		{
			Mockito.when(
					codeExecuteService.executeCode(Mockito.anyString(),
							Mockito.any(CodeExpression.class))).thenThrow(exception);
		} 
		catch (CompilationError | UnknownInterpreter | TimeOutException e) 
		{

		}
	}
	
	@Test
	public void testExecuteServiceCompilationError() throws Exception
	{
		/** Before */
		this.beforeExecuteServiceCompilationError();
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/execute")
				.accept(MediaType.APPLICATION_JSON)
				.content(jsonRequest)
				.contentType(MediaType.APPLICATION_JSON);
		/** When */
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		/** Then */
		JSONAssert.assertEquals(jsonResult, result.getResponse().getContentAsString(),false);
	}
	
	private void beforeExecuteServiceTimeOutException()
	{
		TimeOutException exception = new TimeOutException();
		exception.setMessage(null);
		jsonResult = "{" + 
				"\"status\":\"BAD_REQUEST\"," + 
				"\"message\":\"EXECUTION_TIME_OUT\"," + 
				"\"error\":null" + 
				"}";
		jsonRequest = "{\"code\":\"%python print 1+1\"}";
		try 
		{
			Mockito.when(
					codeExecuteService.executeCode(Mockito.anyString(),
							Mockito.any(CodeExpression.class))).thenThrow(exception);
		} 
		catch (CompilationError | UnknownInterpreter | TimeOutException e) 
		{

		}
	}
	
	@Test
	public void testExecuteServiceTimeOutException() throws Exception
	{
		/** Before */
		this.beforeExecuteServiceTimeOutException();
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/execute")
				.accept(MediaType.APPLICATION_JSON)
				.content(jsonRequest)
				.contentType(MediaType.APPLICATION_JSON);
		/** When */
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		/** Then */
		JSONAssert.assertEquals(jsonResult, result.getResponse().getContentAsString(),false);
	}
}

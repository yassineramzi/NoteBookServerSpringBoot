package com.notebook.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.notebook.core.exceptions.CompilationError;
import com.notebook.core.exceptions.TimeOutException;
import com.notebook.core.exceptions.UnknownInterpreter;
import com.notebook.core.executor.Executor;
import com.notebook.core.executor.ExecutorFactory;
import com.notebook.core.parser.CodeParser;
import com.notebook.models.CodeExpression;
import com.notebook.models.Program;
import com.notebook.models.Result;
import com.notebook.service.CodeExecuteService;
import com.notebook.service.impl.CodeExecuteServiceImpl;

@RunWith(SpringRunner.class)
public class CodeExecuteServiceImplTest 
{
	@TestConfiguration
	static class CodeExecuteServiceImplTestContextConfiguration 
	{
		@Bean
		public CodeExecuteService codeExecuteService()
		{
			return new CodeExecuteServiceImpl();
		}
	}
	
	@MockBean
	private CodeParser codeParser;
	
	@MockBean
	private ExecutorFactory executorFactory;
	
	@MockBean
	private Executor executor;
	
	@Autowired
	private CodeExecuteServiceImpl codeExecuteService;
	
	private Program program;
	
	private String sessionId;
	
	private CodeExpression codeExpression;
	
	private Result result;
	
	private void beforeCodeExecuteSuccess() throws Exception
	{
		program = new Program();
		program.setInterpreter("python");
		result = new Result();
		result.setResult("2");
		sessionId = "1";
		codeExpression = new CodeExpression();
		Mockito
		.when(codeParser.parseCode(codeExpression))
		.thenReturn(program);
		Mockito
		.when(executor.eval(program))
		.thenReturn(result);
		Mockito
		.when(executorFactory.getScriptEngineBySessionId (program.getInterpreter(),sessionId))
		.thenReturn(executor);

	}
	
	@Test
	public void testCodeExecuteSuccess() throws Exception
	{
		/** Before */
		this.beforeCodeExecuteSuccess();
		/** When */
		Result returnedResult = codeExecuteService.executeCode(sessionId, codeExpression);
		/** Then */
		assertEquals(result, returnedResult);
	}
	
	private void beforeCodeExecuteThrowCompilationError() throws Exception
	{
		program = new Program();
		result = new Result();
		sessionId = "1";
		codeExpression = new CodeExpression();
		CompilationError exception = new CompilationError();
		
		Mockito
		.when(codeParser.parseCode(codeExpression))
		.thenReturn(program);
		Mockito
		.when(executor.eval(program))
		.thenThrow(exception);
		Mockito
		.when(executorFactory.getScriptEngineBySessionId (program.getInterpreter(),sessionId))
		.thenReturn(executor);

	}
	
	@Test (expected = CompilationError.class)
	public void testCodeExecuteThrowCompilationError() throws Exception
	{
		/** Before */
		this.beforeCodeExecuteThrowCompilationError();
		/** When */
		codeExecuteService.executeCode(sessionId, codeExpression);
	}
	
	private void beforeCodeExecuteThrowUnknownInterpreter() throws Exception
	{
		program = new Program();
		result = new Result();
		sessionId = "1";
		codeExpression = new CodeExpression();
		UnknownInterpreter exception = new UnknownInterpreter();
		
		Mockito
		.when(codeParser.parseCode(codeExpression))
		.thenReturn(program);
		Mockito
		.when(executor.eval(program))
		.thenThrow(exception);
		Mockito
		.when(executorFactory.getScriptEngineBySessionId (program.getInterpreter(),sessionId))
		.thenReturn(executor);

	}
	
	@Test (expected = UnknownInterpreter.class)
	public void testCodeExecuteThrowUnknownInterpreter() throws Exception
	{
		/** Before */
		this.beforeCodeExecuteThrowUnknownInterpreter();
		/** When */
		codeExecuteService.executeCode(sessionId, codeExpression);
	}
	
	private void beforeCodeExecuteThrowTimeOutException() throws Exception
	{
		program = new Program();
		result = new Result();
		sessionId = "1";
		codeExpression = new CodeExpression();
		TimeOutException exception = new TimeOutException();
		
		Mockito
		.when(codeParser.parseCode(codeExpression))
		.thenReturn(program);
		Mockito
		.when(executor.eval(program))
		.thenThrow(exception);
		Mockito
		.when(executorFactory.getScriptEngineBySessionId (program.getInterpreter(),sessionId))
		.thenReturn(executor);

	}
	
	@Test (expected = TimeOutException.class)
	public void testCodeExecuteThrowTimeOutException() throws Exception
	{
		/** Before */
		this.beforeCodeExecuteThrowTimeOutException();
		/** When */
		codeExecuteService.executeCode(sessionId, codeExpression);
	}
}

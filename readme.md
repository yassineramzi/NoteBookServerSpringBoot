# Project Title

NoteBook Server is an application that expose an end point : 
```
/execute
```
And execute code, that should be in the format :
```
%<interpreter-name><whitespace><code>
```

## Getting Started

### Examples
```
// javascript example 
%javascript print(1)
// python example
%python print 1
```
### Architecture

![alt text](img/architecture_core.JPG)


## CI & CD
### Gitlab CD
For the CD we used the Gitlab CD plateform, it has these pipelines :

#### Stages:
  - test :  pass the unit tests,
  - build : build the project,
  - package : package the artifact in a docker image,
  - deploy : deploy the docker image on heroku.
  
#### Test coverage
In the test stage we can view on the relied job the coverage %:
![alt text](img/couverture_tests.JPG)

#### Deployment into heroku
you can view the application on : https://vast-mountain-63078.herokuapp.com/
